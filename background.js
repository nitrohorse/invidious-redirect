const invidious = 'https://invidious.ggc-project.de';
const excludedPaths = [
	'/playlist'
];

chrome.webRequest.onBeforeRequest.addListener(
	(details) => {
		const url = new URL(details.url);
		
		if (url.hostname === 'invidious.ggc-project.de') {
			return;
		}
		
		for (const path of excludedPaths) {
			if (url.pathname.indexOf(path) === 0) {
				return;
			}
		}
		
		return {
			redirectUrl: `${invidious}${url.pathname}${url.search}${url.hash}`
		};
	},
	{
		urls: [
			'*://youtube.com/*',
			'*://www.youtube.com/*',
			'*://m.youtube.com/*'
		],
		types: [
			'main_frame',
			'sub_frame',
			'stylesheet',
			// 'script',
			'image',
			'object',
			'xmlhttprequest',
			'other'
		]
	},
	['blocking']
);
