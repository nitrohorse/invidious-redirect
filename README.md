# Invidious Redirect

Powered by the open-source code of [Old Reddit Redirect](https://github.com/tom-james-watson/old-reddit-redirect) web extension.

Redirects YouTube links to Invidious, specifically https://invidious.ggc-project.de/. Until [Invidition](https://codeberg.org/Booteille/Invidition) is ported to Chromium this is a scrappy, temporary solution you can run locally.

## Develop and run locally for Chromium
* Open Chromium and navigate to `chrome://extensions/`.
* Enabled "Developer mode" and select "Load unpacked."
* Open the main directory to run the extension.
* Use the [Chrome Apps & Extensions Developer Tool](https://chrome.google.com/webstore/detail/chrome-apps-extensions-de/ohmmkhmmmpcnpikjeljgnaoabkaalbgc?utm_source=chrome-app-launcher-info-dialog) for debugging.

## License

Code copyright Tom Watson. Code released under [the MIT license](LICENSE.txt).
